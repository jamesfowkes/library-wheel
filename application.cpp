#include "raat.hpp"

#include "raat-oneshot-timer.hpp"
#include "raat-oneshot-task.hpp"
#include "raat-task.hpp"

static raat_devices_struct const * pDevices;
static raat_params_struct const * pParams;

static void my_task_fn(RAATTask& ThisTask, void * pTaskData)
{
    (void)ThisTask; (void)pTaskData;

    static int32_t old_count = 0;
    const int32_t new_count = pDevices->pEncoder->count();
    const int32_t threshold = pParams->pThreshold->get();
    const int32_t timeout = pParams->pTimeout->get() * 1000;

    int32_t difference = new_count - old_count;

    bool speed_mode = pParams->pSpeedMode->get();

    raat_logln_P(LOG_APP, PSTR("%" PRIi32 ", %" PRIi32 ", %" PRIi32 ", %" PRIi32), new_count, old_count, difference, threshold);

    if (difference > threshold)
    {
        raat_logln_P(LOG_APP, PSTR("Motion left (%" PRIi32 ")"), difference);
        pDevices->pMotion_Relay->set(true, timeout);
        pDevices->pLeft_Relay->set(true, timeout);
        pDevices->pNone_Relay->set(false, timeout);
        pDevices->pRight_Relay->set(false);
        if (speed_mode) { old_count = new_count; }
    }
    else if (difference < -threshold)
    {
        raat_logln_P(LOG_APP, PSTR("Motion right (%" PRIi32 ")"), difference);
        pDevices->pMotion_Relay->set(true, timeout);
        pDevices->pRight_Relay->set(true, timeout);
        pDevices->pNone_Relay->set(false, timeout);
        pDevices->pLeft_Relay->set(false);
        if (speed_mode) { old_count = new_count; }
    }

    if (!speed_mode)
    {
        old_count = new_count;
    }
}

static RAATTask my_task(500, my_task_fn, NULL);

void raat_custom_setup(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)params;
    pDevices = &devices;
    pParams = &params;

    bool speed_mode = pParams->pSpeedMode->get();
    pDevices->pNone_Relay->set(true);

    raat_log_P(LOG_APP, PSTR("Library Wheel: %s Mode, Threshold %d"), speed_mode ? "Speed" : "Distance", pParams->pThreshold->get());
}

void raat_custom_loop(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)devices; (void)params;
    my_task.run();
}
